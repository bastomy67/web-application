-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 19 Feb 2019 pada 20.26
-- Versi server: 10.1.30-MariaDB
-- Versi PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `my_application`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `avatar` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `password`, `avatar`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'bastomy67@gmail.com', 'bastomy', '$2a$08$BEnKss.esDuMShFkWZz5weTdGhpuVT6Xwg/W6TiFcYx.Ycq50geBm', '/assets/uploads/avatar/bastomy_avatar_1550591239.png', '2019-02-19 15:53:15', '2019-02-19 09:53:15', NULL),
(2, 'bastomy0@gmail.com', 'bastomy tes', '$2a$08$W4SMuYrY2VHO.KRfyH26PeXa.68svuN9PjBuOTzgXxuJgRxp/pYX.', 'assets/uploads/avatar/bastomy0_1550587894/happy-businessman-making-thumbs-up-sign_1325-454.jpg', '2019-02-19 18:26:34', '2019-02-19 12:26:34', NULL),
(8, 'agus@gmail.com', 'agus sujadi', '$2a$08$muX70ohLtnSsXtmIxnalhuekOCVVO61hXfHfgtTZg5yYTqF1t4dPK', 'assets/uploads/avatar/agus sujadi_1550600750/DSC_2039.jpg', '2019-02-19 18:25:50', '2019-02-19 12:25:50', NULL),
(9, 'hendra@gmail.com', 'hendra', '$2a$08$3/ljC8DDeTwaeuGY6.giue/xmdPp25KwubVE5Ng.Mi79pJcQK/kdi', 'assets/uploads/avatar/hendra_1550598824/belajar-di-rumah.jpg', '2019-02-19 11:53:44', '2019-02-19 11:53:44', NULL),
(10, 'jerry@gmail.com', 'jerry', '$2a$08$6WM7iJl6z2OEqQYiAq3Mu.BGM3FelfTiLgROKOwR/p49c5/7TKcxa', 'assets/uploads/avatar/jerry_1550598927/jerri.jpg', '2019-02-19 11:55:27', '2019-02-19 11:55:27', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

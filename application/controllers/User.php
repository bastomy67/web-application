<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
	}

	public function index()
	{
        if ($this->session->userdata('id')==null) {
	   	   $this->load->view('index'); 
        }else{
            redirect(base_url('User/dashboard'),'refresh');
        }
	}

    public function login()
    {
        if ($this->session->userdata('id')==null) {
            $query = $this->UserModel->login($this->input->post('email'));
            if ($query->num_rows()>0) {
                $result = $query->row_array();
                if ($this->bcrypt->check_password($this->input->post('password'), $result['password']))
                {
                    $array = array(
                        'id' => $result['id'],
                        'nama' => $result['name'],
                        'email' => $result['email'],
                        'avatar' => $result['avatar']
                    );
                    $this->session->set_userdata( $array );
                    return print "true";
                }
                else
                {
                    return print "Kata sandi salah";
                }
            }else{
                return print "Email tidak terdaftar";
            }
        }else{
            redirect(base_url('User/dashboard'),'refresh');
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url());
    }

    public function dashboard()
    {
        $data["user"] = $this->UserModel->get_by_id($this->session->userdata('id'))->row();
        $this->load->view('dashboard', $data);
    }

    function get_data_user()
    {
        $list = $this->UserModel->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->name;
            $row[] = $field->email;
            $row[] = "<img src='".base_url($field->avatar)."' style='height:200px;width:200px;'>";
            $row[] = "<button onclick='editUser(".$field->id.")' class='btn btn-warning btn-sm'><i class='fa fa-pencil'></i> Edit</button> <button onclick='deleteUser(".$field->id.")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i> Hapus</button>";

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->UserModel->count_all(),
            "recordsFiltered" => $this->UserModel->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }

	public function registrasi()
	{
        if ($this->session->userdata('id')==null) {
    		$this->load->view('registrasi');
	   }else{
            redirect(base_url('User/dashboard'),'refresh');
       }
    }

	public function registrasiPost()
	{
        if ($this->session->userdata('id')==null) {
    		$dataInsert["email"]    =$this->input->post('email');
    		$dataInsert["name"]     =$this->input->post('name');
    		$dataInsert["email"]    =$this->input->post('email');
    		$dataInsert['password'] = $this->bcrypt->hash_password($this->input->post('password'));
    		$datafoto 				= $this->multiple_upload('avatar/' .$this->input->post('name')."_".time());
            $dataInsert["avatar"]   = substr($datafoto[0]['full_path'], strpos($datafoto[0]['full_path'], 'assets/'));
            if ($this->UserModel->save_data($dataInsert)) {
            	$this->session->set_flashdata('notif', '<div class="alert alert-success text-center"style="width: 100%"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Registrasi Berhasil</div>');
    			redirect(base_url('User'),'refresh');
            }else{
    			$this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="width: 100%"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Registrasi Gagal</div>');
            	redirect(base_url('User/registrasi'),'refresh');
            }
        }else{
            redirect(base_url('User/dashboard'),'refresh');
        }
	}

    public function addUser()
    {
        $dataInsert["name"]     =$this->input->post('name');
        $dataInsert["email"]    =$this->input->post('email');
        $dataInsert['password'] = $this->bcrypt->hash_password($this->input->post('password'));
        $datafoto               = $this->multiple_upload('avatar/' .$this->input->post('name')."_".time());
        $dataInsert["avatar"]   = substr($datafoto[0]['full_path'], strpos($datafoto[0]['full_path'], 'assets/'));
        if ($this->UserModel->save_data($dataInsert)) {
            return print "true";
        }else{
            return print "false";
        }
    }

    public function updateUser()
    {
        $dataUpdate["name"]     =$this->input->post('name');
        if ($this->input->post('password')!="") {
           $dataUpdate['password'] = $this->bcrypt->hash_password($this->input->post('password'));
        }

        $datafoto               = $this->multiple_upload('avatar/' .$this->input->post('name')."_".time());
        $f   = substr($datafoto[0]['full_path'], strpos($datafoto[0]['full_path'], 'assets/'));
        
        if ($f!=null) {
            $dataUpdate['avatar']=$f;
        }
        if ($this->UserModel->update_data($this->input->post('id_user'),$dataUpdate)) {
            return print "true";
        }else{
            return print "false";
        }
    }

    public function checkEmail()
    {
        $data = $this->UserModel->checkEmail($this->input->post('email'))->row_array();
        if (count($data)>0) {
            return print "false";
        }else{
            return print "true";
        }
    }

	public function multiple_upload($upload_dir = 'test', $config = array())
    {
        if ($upload_dir == '') {
            $upload_dir = 'test';
        }
        $user_folder = $upload_dir;
        
        if (!is_dir('uploads/' . $user_folder)) {
            @mkdir('./assets/uploads/' . $user_folder, 0777, TRUE);
        }
        
        
        $files = array();
        
        if (empty($config)) {
            $config['upload_path']   = './assets/uploads/' . $user_folder;
            $config['allowed_types'] = 'zip|gif|jpg|png|pdf';
            $config['max_size']      = 0;
        }
        
        $this->load->library('upload', $config);
        
        $errors = FALSE;
        
        foreach ($_FILES as $key => $value) {
            if (!empty($value['name'])) {
                if (!$this->upload->do_upload($key)) {
                    $data['upload_message'] = $this->upload->display_errors('<p>', '</p>'); // ERR_OPEN and ERR_CLOSE are error delimiters defined in a config file
                    $this->load->vars($data);
                    
                    $errors = TRUE;
                } else {
                    // Build a file array from all uploaded files
                    $files[] = $this->upload->data();
                }
            }
        }
        
        // There was errors, we have to delete the uploaded files
        if ($errors) {
            foreach ($files as $key => $file) {
                @unlink($file['full_path']);
            }
        } elseif (empty($files) AND empty($data['upload_message'])) {
            $this->lang->load('upload');
            $data['upload_message'] = '<p>' . $this->lang->line('upload_no_file_selected') . '</p>';
            $this->load->vars($data);
        } else {
            return $files;
        }
    }

    public function changeAvatar()
    {
        $userName = $this->session->userdata('nama');
        $userName = str_replace(' ','-',$userName);
        $id_user = $this->session->userdata('id');
        $data_photo = $this->input->post('profile');
        $data_photo = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data_photo));
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $data_photo, FILEINFO_MIME_TYPE);
        $mime_split = explode( '/', $mime_type );
        if($mime_split[1]=='jpeg') $mime_split[1] = 'jpg';
        $temp_avatar = $data_photo;

        if($this->session->userdata('avatar') != null){
            $old_avatar = $this->session->userdata('avatar');
            if (file_exists(getcwd().$old_avatar)) unlink(getcwd().$old_avatar);
        }

        $avatar_name = 'avatar_'.time();
        $new_path = '/assets/uploads/avatar/'.$userName.'_'.$avatar_name.'.'.$mime_split[1];
        if(file_put_contents(getcwd().$new_path, $temp_avatar)){
            $dataUpdate['avatar'] = $new_path;
            $this->UserModel->update_data($id_user,$dataUpdate);
        }
        else{
            alert()->error('Foto profil gagal di perbaharui', 'Oops');
        }
        redirect(base_url('User/dashboard'),'refresh');
    }

    public function changePassword()
    {
        $result = $this->UserModel->get_by_id($this->session->userdata('id'))->row_array();
        if ($this->bcrypt->check_password($this->input->post('old_pass'), $result['password']))
        {
            $dataUpdate['password'] = $this->bcrypt->hash_password($this->input->post('password'));
            if ($this->UserModel->update_data($this->session->userdata('id'),$dataUpdate)) {
                $this->session->set_flashdata('notif', '<div class="alert alert-success text-center"style="width: 100%"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>password berhasil diganti</div>');
            }
        }else{
            $this->session->set_flashdata('notif', '<div class="alert alert-danger text-center" style="width: 100%"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>password salah</div>');
        }
        redirect(base_url('User/dashboard'),'refresh');
    }

    public function deleteAccount()
    {
        if ($this->UserModel->delete_data($this->input->post('id'))) {
            echo "true";
        }else{
            echo "false";
        }
    }

    public function getDetail()
    {
        print_r(json_encode($this->UserModel->get_by_id($this->input->post('id'))->row()));
    }

}

/* End of file User.php */
/* Location: ./application/controllers/User.php */
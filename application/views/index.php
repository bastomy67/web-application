<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Web Application</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/dist/css/bootstrap.min.css') ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css')?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url('assets/Ionicons/css/ionicons.min.css')?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.min.css')?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/square/blue.css')?>">
    <link href="<?php echo base_url('assets/css/card.css') ?>" rel="stylesheet">
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        Web Application
      </div>
      <?php echo $this->session->flashdata('notif'); ?>
      <!-- /.login-logo -->
      <div class="card">
  <div class="card-header">Login</div>
  <div class="card-body">
    <div class="form-group has-feedback">
      <input type="text" class="form-control" name="email" id="formEmail" placeholder="Email">
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
    </div>
    <div class="form-group has-feedback">
      <input type="password" class="form-control" name="password" id="formPassword" placeholder="Kata sandi">
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
    </div>
    <div class="row">
      <div class="col-xs-8">

      </div>
      <!-- /.col -->
      <div class="col-xs-4">
        <button type="submit" onclick="login()" class="btn btn-primary btn-block btn-flat">Masuk</button>
      </div>
      <!-- /.col -->
    </div>
    belum punya akun?
    <a href="<?php echo site_url('User/registrasi') ?>">Registrasi</a><br>
  </div>
</div>
      <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <!-- jQuery 3 -->
    <script src="<?php echo base_url('assets/jquery/dist/jquery.min.js')?>"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url('assets/bootstrap/dist/js/bootstrap.min.js')?>"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url('assets/plugins/iCheck/icheck.min.js')?>"></script>
    <script src="<?php echo base_url('assets/vendor/sweetalert/dist/sweetalert.min.js')?>"></script>
    <script type="text/javascript">
      function login() {
        $.post("<?php echo base_url('User/login') ?>",{
          email: $("#formEmail").val(),
          password: $('#formPassword').val()
        },function(data) {
          if (data=="true") {
            swal("Login berhasil").then(function(isConfirm) {
              if (isConfirm) {
                window.location.href=("<?php echo base_url('User/dashboard') ?>");
            }
        });
          }else{
            swal(data);
          }
        });
      }
    </script>
  </body>
</html>
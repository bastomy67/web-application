<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Web Application</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/dist/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/Ionicons/css/ionicons.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/AdminLTE.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/skins/_all-skins.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/dist/css/viewer.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/cropperjs/dist/cropper.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/DataTables/datatables.min.css')?>">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
      .cropper-container.cropper-bg{
      max-width: 100% !important;
      }
      .bs-glyphicons {
      padding-left: 0;
      padding-bottom: 1px;
      margin-bottom: 20px;
      list-style: none;
      overflow: hidden;
      }
      .bs-glyphicons li {
      float: left;
      width: 25%;
      height: 115px;
      padding: 10px;
      margin: 0 -1px -1px 0;
      font-size: 12px;
      line-height: 1.4;
      text-align: center;
      border: 1px solid #ddd;
      }
      .bs-glyphicons .glyphicon {
      margin-top: 5px;
      margin-bottom: 10px;
      font-size: 24px;
      }
      .bs-glyphicons .glyphicon-class {
      display: block;
      text-align: center;
      word-wrap: break-word; /* Help out IE10+ with class names */
      }
      .bs-glyphicons li:hover {
      background-color: rgba(86, 61, 124, .1);
      }
      @media (min-width: 768px) {
      .bs-glyphicons li {
      width: 12.5%;
      }
      }
    </style>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo site_url('User/dashboard') ?>" class="logo">
        <span class="logo-mini"><b>M</b>P</span>
        <span class="logo-lg">Web Application</span>
        </a>
        <nav class="navbar navbar-static-top">
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li>
                <a href="#" onclick="Logout()" data-toggle="control-sidebar">Logout</a>
              </li>
              <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-gears"></i>
                </a>
                <ul class="dropdown-menu">
                  <li class="header">Pengaturan</li>
                  <li>
                    <ul class="menu">
                      <li>
                        <a href="javascript:void(0)" onclick="editUser(<?php echo $user->id ?>)">
                          <h3>
                            Edit Profil
                          </h3>
                        </a>
                      </li>
                      <li>
                        <a href="#" data-toggle="modal" data-target="#update-profile-photo">
                          <h3>
                            Ganti Foto Profil
                          </h3>
                        </a>
                      </li>
                      <li>
                        <a href="#" data-toggle="modal" data-target="#update-password">
                          <h3>
                            Ganti Kata Sandi
                          </h3>
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <aside class="main-sidebar">
        <section class="sidebar">
          <div class="user-panel">
            <div class="pull-left image">
                <img id="foto_profile_view" src="<?php echo base_url().$user->avatar ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $user->name ?></p>
            </div>
          </div>
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU NAVIGASI</li>
            <li class="active'">
              <a href="#"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            </li>
          </ul>
        </section>
      </aside>
      <div class="content-wrapper">
        <?php echo $this->session->flashdata('notif'); ?>
      <section class="content-header">
        <h1>
          Daftar User
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo base_url('User/dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Daftar User</li>
        </ol>
      </section>
      <section class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-header">
                  <div class="row">
                  <div class="col-md-10">
                    <h3 style="margin-top: 0px">Daftar Timses</h3>
                  </div>
                  <div class="col-md-2">  
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUser"><i class="fa fa-plus"></i>  Tambah User</button>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <table id="table" class="table table-bordered table-striped" style="width: 100%;">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Avatar</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Avatar</th>
                        <th>Aksi</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
      </div>
      <!-- Modal -->
      <div id="editUser" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah User</h4>
              </div>
              <div class="modal-body">
                <input type="hidden" name="id_user" id="id_user">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="name" id="editName" class="form-control" placeholder="Nama" required>
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" id="editPassword" class="form-control" placeholder="Password">
                  <span class="help-block">Kosongkan jika tidak ingin mengganti</span>
                </div>
                <div class="form-group header-file">
                  <label>Avatar</label>
                  <input type="file" name="avatar" id="editAvatar" class="form-control myFile" placeholder="Avatar" accept="image/*"/ required>
                  <span class="help-block">Kosongkan jika tidak ingin mengganti</span>
                  <span class="help-block toBig" style="color: red; display: none;">File terlalu besar, maksimal 2 MB </span>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn-edit-submit" class="btn btn-success">Update</button>
              </div>
            </div>
        </div>
      </div>

      <!-- Modal -->
      <div id="addUser" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tambah User</h4>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" name="name" id="formName" class="form-control" placeholder="Nama" required>
                </div>
                <div class="form-group" id="form-email">
                  <label>Email</label>
                  <input type="text" name="email" id="formEmail" class="form-control" placeholder="Email" required>
                  <span class="help-block" id="span_email" style="color: red; display: none;">Email telah terdaftar</span>
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" id="formPassword" class="form-control" placeholder="Password" required>
                </div>
                <div class="form-group header-file">
                  <label>Avatar</label>
                  <input type="file" name="avatar" id="formAvatar" class="form-control myFile" placeholder="Avatar" accept="image/*"/ required>
                  <span class="help-block toBig" style="color: red; display: none;">File terlalu besar, maksimal 2 MB </span>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" id="btn-submit" class="btn btn-success">Simpan</button>
              </div>
            </div>
        </div>
      </div>

      <!-- Modal -->
      <div id="update-profile-photo" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Update Avatar</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <button type="button" class="btn btn-primary btn-sm pull-right" id="button-profile" style="margin: 5px"><i class="fa fa-save"></i> Simpan</button>
                  <label class="btn btn-secondary pull-right" style="margin: 5px">
                  <i class="fa fa-search"></i> Pilih Foto 
                  <input type="file" name="file" class="btn btn-primary" onchange="changeProfile(this.files)" accept="image/*" style="display: none;">
                  </label>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12" style="margin-top: 10px">
                  <center>
                    <img class="image" id="imageProfile" src="<?php echo base_url().$user->avatar ?>">
                  </center>
                  <form id="form-upload-profile" action="<?php echo base_url('User/changeAvatar') ?>" method="post">
                    <input type="hidden" name="profile" class="form-control form-control-flat" id="cropOutputProfile">
                  </form>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal -->
      <div id="update-password" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <!-- Modal content-->
          <div class="modal-content">
            <form action="<?php echo base_url('User/changePassword') ?>" method="POST">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Perbaharui Kata Sandi</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label>Kata sandi lama</label>
                <input type="password" class="form-control" name="old_pass" placeholder="Kata sandi lama" required="required">
              </div>
              <div class="form-group">
                <label>Kata sandi baru</label>
                <input type="password" class="form-control" name="password" id="password1" placeholder="Kata sandi baru" required="required">
              </div>
              <div class="form-group">
                <label>Konfirmasi kata sandi baru</label>
                <input type="password" class="form-control" name="re-passsword" id="password2" placeholder="Konfirmasi kata sandi baru" required="required">
                <span class="help-block" id="message_error_password" style="color: red; display: none;">Kata sandi tidak cocok</span>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-success" id="submitPass">Simpan</button>
            </div>
            </form>
          </div>
        </div>
      </div>

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 0.1
        </div>
        <strong>&copy; 2018 <a href="https://www.mediopolitico.com" target="_blank">Medio Politico</a>.</strong> All rights reserved.
      </footer>
      <div class="control-sidebar-bg"></div>
    </div>
    <script src="<?php echo base_url('assets/jquery/dist/jquery.min.js')?>"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url('assets/bootstrap/dist/js/bootstrap.min.js')?>"></script>

    <script src="<?php echo base_url('assets/dist/js/adminlte.min.js')?>"></script>
    <script src="<?php echo base_url('assets/dist/js/viewer.js')?>"></script>
    <script src="<?php echo base_url('assets/vendor/sweetalert/dist/sweetalert.min.js')?>"></script>
    <script src="<?php echo base_url('assets/plugins/cropperjs/dist/cropper.js')?>"></script>
    <script src="<?php echo base_url('assets/js/imagesloaded.pkgd.js')?>"></script>
    <script src="<?php echo base_url('assets/DataTables/datatables.min.js')?>"></script>
    <script type="text/javascript">
      function Logout(){
        swal({
          title: "Anda yakin keluar dari aplikasi?",
          text: "Jika anda keluar maka anda harus login kembali",
          icon: "warning",
          buttons: true,
          dangerMode: true,
          confirmButtonText: "Ya",
          cancelButtonText: "Batal"
        }).then(function(isConfirm) {
           if (isConfirm) {
              window.location.href=("<?php echo base_url('User/logout') ?>");
           }
        });
      }

      $("#btn-submit").click(function(e){
        e.stopPropagation();
        e.preventDefault();
        var field_name = $("#formName").val();
        var field_email= $("#formEmail").val();
        var field_password = $("#formPassword").val();
        var field_avatar = $('#formAvatar').prop('files')[0]; 
        var form_data = new FormData();
        form_data.append('file',field_avatar);
        form_data.append('name',field_name);
        form_data.append('email',field_email);
        form_data.append('password',field_password);
        $("#btn-submit").prop("disabled", true);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "<?php echo base_url('User/addUser') ?>",
            dataType: 'text',
            data: form_data ,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                if (data=="true") {
                  swal({
                       title: "Saved!",
                       text: "Akun berhasil ditambahkan.",
                       type: "success"
                     }).then(function(ok){
                      if (ok){
                        location.reload();
                      }
                     });
                }else{
                  swal({
                     title: "Gagal!",
                     text: "Akun gagal ditambahkan.",
                     type: "success"
                  });
                }
                $("#btn-submit").prop("disabled", false);
                $("#addUser").modal('toggle');
            },
            error: function (e) {
                console.log("ERROR : ", e);
                $("#btn-submit").prop("disabled", false);

            }
        });
      }); 

      $("#btn-edit-submit").click(function(e){
        e.stopPropagation();
        e.preventDefault();
        var field_id = $("#id_user").val();
        var field_name = $("#editName").val();
        var field_password = $("#editPassword").val();
        var field_avatar = $('#editAvatar').prop('files')[0]; 
        var form_data = new FormData();
        form_data.append('id_user',field_id);
        form_data.append('file',field_avatar);
        form_data.append('name',field_name);
        form_data.append('password',field_password);
        $("#btn-edit-submit").prop("disabled", true);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "<?php echo base_url('User/updateUser') ?>",
            dataType: 'text',
            data: form_data ,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                if (data=="true") {
                  swal({
                       title: "Saved!",
                       text: "Akun berhasil diperbaharui.",
                       type: "success"
                     }).then(function(ok){
                      if (ok){
                        location.reload();
                      }
                     });
                }else{
                  swal({
                     title: "Gagal!",
                     text: "Akun gagal diperbaharui.",
                     type: "success"
                  });
                }
                $("#btn-submit").prop("disabled", false);
                $("#editUser").modal('toggle');
            },
            error: function (e) {
                console.log("ERROR : ", e);
                $("#btn-submit").prop("disabled", false);

            }
        });
      });

      $("#formEmail").keyup(function(){
        var value = $(this).val();
        $.post("<?php echo base_url('User/checkEmail') ?>", {
           email:value
         }, 
         function(data) {
          if (data=="false") {
            $("#btn-submit").prop('disabled',true);
            $("#form-email").addClass("has-error");
            $("#span_email").show();
          }else{
            $("#btn-submit").prop('disabled',false);
            $("#form-email").removeClass("has-error");
            $("#span_email").hide();
          }
         });
      });

      $("#password2").keyup(function(){
        if($("#password1").val() != $("#password2").val()){
          $("#submitPass").prop('disabled',true);
          $("#message_error_password").show();
        }else{
          $("#submitPass").prop('disabled',false);
          $("#message_error_password").hide();
        }
      });

      $('.myFile').bind('change', function() {
        var size = (this.files[0].size/1024);
        if(size>=2000){
          $(".toBig").show();
          $(".btn-success").prop('disabled',true);
          $(".header-file").addClass("has-error");
        }else{
          $(".toBig").hide();
          $(".btn-success").prop('disabled',false);
          $(".header-file").attr('class', 'form-group');
        }
      });

      function editUser(id) {
        $.post("<?php echo base_url('user/getDetail') ?>", {
          id:id
        },function(data){
          var obj = $.parseJSON(data);
          $("#id_user").val(obj.id);
          $("#editName").val(obj.name);
        });
        $("#editUser").modal("show");
      }

      function deleteUser(id) {
        swal({
           title: "Apakah anda yakin?",
           text: "Jika anda menghapus akun ini akan terhapus secara permanen.",
           icon: "warning",
           buttons: true,
           dangerMode: true,
        }).then(function(willDelete){
          if (willDelete) {
            $.post("<?php echo base_url('User/deleteAccount') ?>", {
                   id: id
                 },
                 function(data) {
                  if (data=="true") {
                     swal({
                       title: "Terhapus!",
                       text: "Akun berhasil terhapus.",
                       type: "success"
                     }).then(function(ok){
                      if (ok){
                        location.reload();
                      }
                     });
                  }else{
                    swal({
                       title: "Gagal!",
                       text: "Akun gagal terhapus.",
                       type: "success"
                     })
                  }
                 }
               );
           }else{
              swal("Akun anda aman!");
           }
        });
      }
    </script>
    <script type="text/javascript">
    var table;
    $(document).ready(function() {
 
        //datatables
        table = $('#table').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo base_url('User/get_data_user')?>",
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
        });
    });
    </script>
    <script type="text/javascript">
      var croppable = true;
      var cropperProfile = new Cropper(imageProfile, {
        aspectRatio: 1,
        viewMode: 1,
        ready: function () {
          croppable = true;
        }
      });
      
      function changeProfile(files){
        var filesToUpload = files;
        var file = filesToUpload[0];
        if(file){
        var reader = new FileReader();
            reader.onload = function(e) {
              imageProfile.src = e.target.result;
              cropperProfile.replace(imageProfile.src);
            }
            reader.readAsDataURL(file);
        }
      }
      (function () {
      
      function getRoundedCanvasProfile(sourceCanvas) {
        var canvas = document.createElement('canvas');
        var context = canvas.getContext('2d');
        var width = sourceCanvas.width;
        var height = sourceCanvas.height;
      
        var MAX_WIDTH = 500;
        var MAX_HEIGHT = 500;
      
        if (width > height) {
          if (width > MAX_WIDTH) {
            height *= MAX_WIDTH / width;
            width = MAX_WIDTH;
          }
        } else {
          if (height > MAX_HEIGHT) {
            width *= MAX_HEIGHT / height;
            height = MAX_HEIGHT;
          }
        }
      
        canvas.width = width;
        canvas.height = height;
        canvas.width = width;
        canvas.height = height;
        context.beginPath();
        context.rect(0, 0, width, height);
        context.strokeStyle = 'rgba(0,0,0,0)';
        context.stroke();
        context.clip();
        context.drawImage(sourceCanvas, 0, 0, width, height);
      
        return canvas;
      }
      
      window.addEventListener('DOMContentLoaded', function () {
        var imageProfile = document.getElementById('imageProfile');
        var buttonProfile = document.getElementById('button-profile');
        var result = document.getElementById('result');
      
        buttonProfile.onclick = function () {
          var croppedCanvas;
          var roundedCanvas;
          var roundedImage;
      
          if (!croppable) {
            return;
          }
      
          // Crop
          croppedCanvas = cropperProfile.getCroppedCanvas();
      
          // Round
          roundedCanvas = getRoundedCanvasProfile(croppedCanvas);
      
          // Show
          roundedImage = document.createElement('img');
          roundedImage.src = roundedCanvas.toDataURL("image/png")
          // result.innerHTML = '';
          // result.appendChild(roundedImage);
          $("#cropOutputProfile").val(roundedImage.src);
          $("#form-upload-profile" ).submit();
        };
      
      });
      
      })();
    </script>
  </body>
</html>